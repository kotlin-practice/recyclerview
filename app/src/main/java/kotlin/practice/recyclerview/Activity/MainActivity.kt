package com.practice.recyclerview.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import com.practice.recyclerview.Adapter.DataAdapter
import com.practice.recyclerview.ModelClass.DataModelItem
import com.practice.recyclerview.R
import com.practice.recyclerview.RetrofitCall.ApiUtils.Companion.getRetrofit
import com.practice.recyclerview.RetrofitCall.RetrofitInterfce

class MainActivity : AppCompatActivity() {

    private lateinit var header: TextView
    private lateinit var dataRecycler: RecyclerView

    private lateinit var retrofitInterfce: RetrofitInterfce

    private var list = ArrayList<DataModelItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initt()

        getData()
    }

    private fun getData() {
        val call: Call<ArrayList<DataModelItem>>? = retrofitInterfce.getData()
        call?.enqueue(object : Callback<ArrayList<DataModelItem>> {
            override fun onResponse(
                    call: Call<ArrayList<DataModelItem>>,
                    response: Response<ArrayList<DataModelItem>>
            ) {

                list = response.body()!!

                val adapter: DataAdapter = DataAdapter(this@MainActivity, list)
                dataRecycler.adapter = adapter

            }

            override fun onFailure(call: Call<ArrayList<DataModelItem>>, t: Throwable) {
            }
        })

    }

    private fun initt() {
        header = findViewById(R.id.header)
        dataRecycler = findViewById(R.id.recyclerView)
        dataRecycler.layoutManager = LinearLayoutManager(this)
        retrofitInterfce = getRetrofit(this).create(RetrofitInterfce::class.java)
    }

}