package com.practice.recyclerview.ModelClass

data class DataModelItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int


)