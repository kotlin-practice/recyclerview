package com.practice.recyclerview.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.practice.recyclerview.ModelClass.DataModelItem
import com.practice.recyclerview.R

class DataAdapter(private val context: Context,private val list: ArrayList<DataModelItem>) :
    RecyclerView.Adapter<DataAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val  view:View = LayoutInflater.from(context).inflate(R.layout.model_layout,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.idTv.setText(""+list.get(position).id)
        holder.useridTv.setText(""+list.get(position).userId)
        holder.titleTv.setText(list.get(position).title)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder (itemView: View):RecyclerView.ViewHolder(itemView){
         val idTv:TextView = itemView.findViewById(R.id.idTv)
         val useridTv:TextView = itemView.findViewById(R.id.useridTv)
         val titleTv:TextView = itemView.findViewById(R.id.titleTv)
    }

}