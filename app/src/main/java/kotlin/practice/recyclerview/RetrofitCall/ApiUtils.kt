package com.practice.recyclerview.RetrofitCall

import android.content.Context
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiUtils {
    companion object {

        var BASE_URL = "https://jsonplaceholder.typicode.com/"

        fun getRetrofit(context: Context): Retrofit {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

    }
}