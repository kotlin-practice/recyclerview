package com.practice.recyclerview.RetrofitCall

import retrofit2.Call
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import java.util.ArrayList
import com.practice.recyclerview.ModelClass.DataModelItem

interface RetrofitInterfce {

    @GET("posts")
    fun getData(): Call<ArrayList<DataModelItem>>

}